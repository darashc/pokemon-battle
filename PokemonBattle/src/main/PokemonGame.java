
package main;

import clase.Pokemon;
import clase.Pokemon.PokemonType;
import clase.User;
import clase.User.AccessLevel;
import exceptions.InvalidGenreException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;



public class PokemonGame {

    
    public static void main(String[] args) {
        // Variables needed to work
        Scanner s=new Scanner(System.in);
        Connection connection=null;
        byte opt=0;
        String menu="\nChoose an action:"
                + "\n\t0 - Exit game"
                + "\n\t1 - Register as new user"
                + "\n\t2 - Log in as existing user";
        User user=null;
        
        try {
            connection=DriverManager.getConnection("jdbc:mysql://85.214.120.213:3306/pokemonbattle1dam", "1dam", "1dam");
        } catch (SQLException ex) {
            System.err.println("Connection to database failed");
            ex.printStackTrace();
        }
        
        do {
            if (user!=null) {
                menu="\nHello, "+user.getName()+"\nChoose an action:";
                if (user.getPokemon()==null) {
                    menu+="\n\t0 - Exit"
                            + "\n\t1 - Register new Pokemon";
                } else {
                    // 11 - Concatenate another option 1 - Show pokemon
                    menu+="\n\t0 - Exit"
                            + "\n\t1 - Show Pokemon";
                }
            }
            System.out.println(menu);
            opt=Byte.parseByte(s.nextLine());
            switch(opt) {
                case 0:
                    System.out.println("Bye");
                    break;
                case 1:
                    // if user is still null, the user hasn't been registered
                    if (user==null) {
                        user=registerUser(s, connection);
                    } else {
                        if (user.getPokemon()==null) {
                            System.out.println("Let's register your Pokemon!");
                            registerPokemon(s, connection, user);
                        } else {
                            // 12 - Modify the switch (case 1) and if it finds that user is not null and user.getPokemon()!=null, execute a new fuction in pokemon class (public static String print()), that prints the pokemon data
                            System.out.println(user.getPokemon().getPokemonData(user.getPokemon()));
                        }
                    }
                    break;
                case 2:
                    user=loginUser(s, connection);
                    break;
                default:
                    System.err.println("Incorrect option. Please try again.");
                    break;
            }
        } while (opt!=0);
    }
    
    public static User registerUser(Scanner s, Connection c) {        
        try {
            System.out.println("Insert your username");
            String name=s.nextLine();
            System.out.println("Choose your password");
            String password=s.nextLine();
            System.out.println("Choose your genre (M/F)");
            char genre=s.nextLine().charAt(0);
            System.out.println("Tell us a bit about yourself");
            String desc=s.nextLine();
            User actual=new User(name, genre, desc, password, AccessLevel.BASIC, null);
            // We're going to insert the user in the database, so we can login as him/her later
            Statement registerStatement=c.createStatement();
            registerStatement.executeUpdate("INSERT INTO user (name, genre, description, password, lastConnection, accessLevel)"
                    + "VALUES ('"+name+"', '"+genre+"', '"+desc+"', '"+password+"', "+(actual.getLastConnection()!=null?"'"+actual.getLastConnection()+"'":"null")+ ", 'Basic')");
            return actual;
            } catch (InvalidGenreException ex) {
                System.err.println(ex.getMessage());
                registerUser(s, c);
            } catch (SQLException ex) {
                System.err.println("SQL exception: Error inserting user into database");
                ex.printStackTrace();
        }
        
        return null;
    }
    
    public static User loginUser(Scanner s, Connection c) {
        try {
            System.out.println("Username:");
            String name=s.nextLine();
            System.out.println("Password:");
            String password=s.nextLine();
            
            // First thing to do would be to retrieve the whole user info from db
            // We're using the wildcard (something you can replace by anything) ? in the following statement.
            // The String array is used to give values to the wildcard as they appear (position 0 replaces the first ?, position 1 replaces the second ? and so on...)
            PreparedStatement loginStatement=c.prepareStatement("SELECT *"
                    + " FROM user"
                    + " WHERE name=? AND password=?");
            loginStatement.setString(1, name);
            loginStatement.setString(2, password);
            ResultSet foundUser=loginStatement.executeQuery();
            if (foundUser.next()) { // If user is found
                System.out.println("Logged in! :)");
                // Once we have found our user, we put him into a Java Object
                AccessLevel al=null;
                if (foundUser.getString("accessLevel").equals("Basic")) {
                    al=AccessLevel.BASIC;
                } else if (foundUser.getString("accessLevel").equals("Premium")) {
                    al=AccessLevel.PREMIUM;
                } else {
                    System.err.println("Invalid Access Level on database");
                }
                
                User actual=new User(foundUser.getString("name"), foundUser.getString("genre").charAt(0), foundUser.getString("description"), foundUser.getString("password"), al, null);
                System.out.println("User details:"
                        +"\n\tName: "+actual.getName()
                        +"\n\tGenre: "+actual.getGenre()
                        +"\n\tDescription: "+actual.getDescription()
                        +"\n\tAccess Level: "+actual.getLevel());
                
                Pokemon pokemonNew=null;
                // 7 - Create or reuse a Statement to query pokemon linked to the user
                Statement selectStatement=c.createStatement();
                ResultSet pokemonId=selectStatement.executeQuery("SELECT pokemonId"
                        + " FROM pokemonUser"
                        + " WHERE user='"+actual.getName()+"'");
                if (pokemonId.next()) { // If user has pokemon
                    // 8 - Create or reuse a Statement to query all pokemon data for the recovered pokemonId
                    ResultSet pokemonData=selectStatement.executeQuery("SELECT *"
                        + " FROM pokemon"
                        + " WHERE id="+pokemonId.getInt("pokemonId")+"");

                    if (pokemonData.next()) { // If said pokemon is in database
                        // 9 - Create a Pokemon java object with all recovered data from the previous step (or null if the user doesn't have any)
                        PokemonType pt=null;
                        switch (pokemonData.getString("type")) {
                            case "FIRE":
                                pt=PokemonType.FIRE;
                                break;
                            case "WATER":
                                pt=PokemonType.WATER;
                                break;
                            case "PLANT":
                                pt=PokemonType.PLANT;
                                break;
                            default:
                                System.err.println("Pokemon Type Invalid");
                        }
                        pokemonNew=new Pokemon(pokemonData.getInt("id"), pokemonData.getString("name"), (pokemonData.getBoolean("genre")==false?'M':'F'), pokemonData.getString("description"), pt, pokemonData.getString("species"), pokemonData.getShort("lifePoints"));
                    }
                } else {
                    System.out.println("User has no pokemon in database");
                }

                // 10 - (only if 9 isn't null) Use the newly created Pokemon variable in the User's constructor last argument
                if (pokemonNew!=null) {
                    actual.setPokemon(pokemonNew);
                }
                
                return actual;
            } else { // If user is not found
                System.err.println("Invalid username/password");
                return loginUser(s, c);
            }
        } catch (SQLException ex) {
            System.err.println("SQL Exception:");
            ex.printStackTrace();
        } catch (InvalidGenreException ex) {
            // This should never happen because we made sure that when we register the user, they couldn't insert the wrong genre
            System.err.println(ex.getMessage());
            System.err.println("Holy Shit! This should have never happened!");
            // We use an assert because this theoretically shpuld never happen
            assert true:"Holy Shit! This should have never happened!";
        }
        return null;
    }
    
    public static void registerPokemon(Scanner sc, Connection c, User user) {
        boolean generobd; // Female is true, Male is false
        try {
            //1 - Ask for all of pokemon data via Scanner
            System.out.println("Name of the Pokemon:");
            String nPokemon=sc.nextLine();
            System.out.println("Gender of the pokemon");
            char gPokemon=sc.nextLine().charAt(0);
            if (gPokemon=='M'||gPokemon=='m') { // Converts the char introduced to boolean, for database purposes
                generobd=false;
            } else {
                generobd=true;
            }
            System.out.println("Species of the Pokemon:");
            String sPokemon=sc.nextLine();
            System.out.println("Type of the Pokemon:");
            String tPokemon=sc.nextLine();
            PokemonType type=null;
            switch (tPokemon) {
                case "Fire":
                case "fire":
                    type=PokemonType.FIRE;
                    break;
                case "Water":
                case "water":
                    type=PokemonType.WATER;
                    break;
                case "Plant":
                case "plant":
                    type=PokemonType.PLANT;
                    break;
                default:
                    System.err.println("Pokemon Type Invalid");
                    registerPokemon(sc, c, user);
            }
            System.out.println("Life Points of the Pokemon:");
            short lpPokemon=Short.parseShort(sc.nextLine());
            System.out.println("Tell us a bit about the Pokemon:");
            String dPokemon=sc.nextLine();
            
            //2 - Create an statement object and insert into pokemon table.  (you can skip id , as it is auto-incremented)
            Statement insertStatement=c.createStatement();
            insertStatement.executeUpdate("INSERT INTO pokemon (name, genre, description, type, species, lifePoints) VALUES"
                    + " ('"+nPokemon+"', "+generobd+", '"+dPokemon+"', '"+type+"', '"+sPokemon+"', '"+lpPokemon+"')");
            
            //3 - Query the newly created pokemon id (query max id from pokemon table)
            ResultSet idPokemon=insertStatement.executeQuery("SELECT MAX(id) id "
                    + " FROM pokemon");
            if (idPokemon.next()) { // if Pokemon is found
                //4 - Create a java Pokemon object with the read data and the queried id
                Pokemon registeredPokemon=new Pokemon(idPokemon.getInt("id"), nPokemon, gPokemon, dPokemon, type, sPokemon, lpPokemon);
            
                //5 - User setPokemon in User class to Link the pokemon to the user in java
                user.setPokemon(registeredPokemon);

                //6 - Reuse Statement or create a new one to insert into pokemonUser table, where you link pokemon and user.
                insertStatement.execute("INSERT INTO pokemonUser VALUES"
                        + "('"+user.getName()+"', "+registeredPokemon.getId()+")");
            } else {
                System.err.println("Error inserting Pokemon into the database, please try again");
            }                   
            
        } catch (SQLException ex) {
            System.err.println("Error inserting Pokemon into the database, please try again");
            ex.printStackTrace();
        } catch (InvalidGenreException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
