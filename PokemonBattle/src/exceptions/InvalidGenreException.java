
package exceptions;


public class InvalidGenreException extends Exception {
    public InvalidGenreException(String msg) {
        super(msg);
    } 
}
