
package clase;

import exceptions.InvalidGenreException;
import java.time.LocalDateTime;


public class User extends LivingBeing {
    private String password;
    private LocalDateTime lastConnection;
    private int timesLoggedIn;
    private AccessLevel level;
    private Pokemon pokemon;
    
    public enum AccessLevel {
        PREMIUM,
        BASIC
    }

    public User(String name, char genre, String description, String password, AccessLevel level, Pokemon pokemon) throws InvalidGenreException {
        super(name, genre, description);
        this.password = password;
        this.lastConnection = null;
        this.timesLoggedIn = 0;
        this.level = level;
        this.pokemon = pokemon;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(LocalDateTime lastConnection) {
        this.lastConnection = lastConnection;
    }

    public int getTimesLoggedIn() {
        return timesLoggedIn;
    }

    public void setTimesLoggedIn(int timesLoggedIn) {
        this.timesLoggedIn = timesLoggedIn;
    }

    public AccessLevel getLevel() {
        return level;
    }

    public void setLevel(AccessLevel level) {
        this.level = level;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }
    
    
}
