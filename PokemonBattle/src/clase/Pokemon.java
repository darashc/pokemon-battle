
package clase;

import exceptions.InvalidGenreException;


public class Pokemon extends LivingBeing {
    private int id;
    private PokemonType type;
    private String species;
    private byte level;
    private short lifePoints;
    private int xp;
    
    public enum PokemonType {
        FIRE,
        WATER,
        PLANT
    }
    
    /**
     * Pokemon basic constructor with all the necessary data
     * @param id numeric id
     * @param name name
     * @param genre gender
     * @param description description
     * @param type type (PokemonType)
     * @param species species
     * @param lifePoints lifepoints (0-100)
     * @throws InvalidGenreException It will throw an exception in case of an invalid genre
     */
    public Pokemon(int id, String name, char genre, String description, PokemonType type, String species, short lifePoints) throws InvalidGenreException {
        super(name, genre, description);
        this.id =id;
        this.type=type;
        this.species = species;
        this.level = 1;
        this.xp = 0;
        this.lifePoints = lifePoints;
    }

    public PokemonType getType() {
        return type;
    }

    public void setType(PokemonType type) {
        
    }

    public byte getLevel() {
        return level;
    }

    public void setLevel(byte level) {
        this.level = level;
    }

    public short getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(short lifePoints) {
        this.lifePoints = lifePoints;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getPokemonData(Pokemon p) {
        String data="";
        
        data+="Your pokemon:"
                + "\n\tID: "+p.getId()
                + "\n\tName: "+p.getName()
                + "\n\tGenre: "+p.getGenre()
                + "\n\tDescription: "+p.getDescription()
                + "\n\tPokemon Type: "+p.getType()
                + "\n\tSpecies: "+p.getSpecies()
                + "\n\tLife Points: "+p.getLifePoints();
        
        return data;
    }
    
}
