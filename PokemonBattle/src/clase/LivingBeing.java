
package clase;

import exceptions.InvalidGenreException;


public class LivingBeing {
    private String name;
    private boolean genre; // Female is represented as true, Men is represented by false
    private String description;

    public LivingBeing(String name, char genre, String description) throws InvalidGenreException {
        this.name = name;
        this.setGenre(genre);
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getGenre() {
        if (this.genre==true) {
            return 'F';
        } else {
            return 'M';
        }
    }

    public final void setGenre(char genre) throws InvalidGenreException {
        switch (genre) {
            case 'F':
            case 'f':
                this.genre=true;
                break;
            case 'M':
            case 'm':
                this.genre=false;
                break;
            default:
                throw new InvalidGenreException(genre+" is not a valid genre (M/F)");
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
